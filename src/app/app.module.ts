import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { modules } from './modules';
import { routes } from './routes';
import { providers } from './providers';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ...routes,
    ...modules
    ],
    providers: [
    ...providers
    ],
  declarations: [
    AppComponent
  ],
  bootstrap: [ AppComponent ]
})
    
export class AppModule { }
