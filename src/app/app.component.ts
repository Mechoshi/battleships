import { SocketService } from './modules/shared/services/socket.service';
import { Component } from '@angular/core';

import './../../styles/main.scss';

@Component( {
    selector: 'app',
    templateUrl: './app.component.html'
} )
export class AppComponent {
    public games: any[] = [];
    public gameName: string = '';
    public showTheGames: boolean = false;

    constructor( private socketService: SocketService ) {
        // console.log(process.env.ENV);
        // console.log(process.env.API_URL);
        this.socketService.initSocket();

        this.socketService.socket.on( 'getGames', ( data: any ) => {
            this.games = data;
        } )

        this.socketService.socket.on( 'newGameAdded', ( data: any ) => {
            this.games = data;
        } )
    }


    public createGame( value: string ): void {
        this.socketService.socket.emit( 'createGame', { name: value, id: this.socketService.socket.id } );
    }

    public joinGame( name: string ): void {

        for ( let i = 0; i < this.games.length; i++ ) {
            if ( this.games[ i ].player1 === this.socketService.socket.id ) {
                console.log( 'Its your game' );
                return;
            }
        }
        this.socketService.socket.emit( 'joinGame', { name: name, id: this.socketService.socket.id } );
    }

    public showGames( msg: any ): void {
        if ( msg.msg === 'showGames' ) {
            this.showTheGames = true;
        }
    }
}