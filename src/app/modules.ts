import { SharedModule } from './modules/shared/shared.module';
import { ShipsModule } from './modules/ships/ships.module';

export const modules: any[] = [
    SharedModule,
    ShipsModule
]