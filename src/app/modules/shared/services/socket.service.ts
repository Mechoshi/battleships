import { EventEmitter, Injectable } from '@angular/core';
const io = require( "socket.io-client" );

@Injectable()
export class SocketService {
    private url: string = 'localhost:3000/'
    public socket: any = null;
    public messageFromServer: any;

    public initSocket(): void {
        try {
            this.socket = io.connect( this.url );
            console.log( 'Socket initiated' );
        } catch ( error ) {
            console.error( error );
        }

        if ( this.socket !== null ) {
            this.socket.on( 'message', ( data: any ) => {
                this.messageFromServer = data;
            } )
        }

    }

    public onInput( input: any ): void {
        const data = input.value;
        console.log( data );
        
        this.socket.emit( 'fromClient', data );
    }


}