
import {
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    OnDestroy,
    Output,
    Renderer,
    ViewChild
} from '@angular/core';

import { ShipService } from '../../services/ship.service';

@Component( {
    selector: "mp-canvas",
    templateUrl: "./canvas.component.html",
    host: {
        'class': 'canvas-container'
    }
} )
export class CanvasComponent implements AfterViewInit, OnDestroy {
    private shipQuantities: number[] = [ 4, 3, 2, 1 ];
    private X_LABELS: string[] = [ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J' ];
    private RECT_SIZE: number = 40;
    private RECT_PER_SIDE: number = 10;

    private canvas: HTMLCanvasElement;
    private context: CanvasRenderingContext2D;
    private canvasClickListener: Function;
    private canvasDragoverListener: Function;
    private canvasDropListener: Function;
    private waterRectangles: any[] = [];

    private ships: any[] = [];

    @Output() readyToChooseGame: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild( 'canvasElement' ) canvasRef: ElementRef;


    constructor(
        private renderer: Renderer,
        private shipService: ShipService
    ) { }

    public ngAfterViewInit(): void {
        this.canvas = this.canvasRef.nativeElement;
        this.renderer.setElementProperty( this.canvas, 'width', 400 );
        this.renderer.setElementProperty( this.canvas, 'height', 400 );
        this.context = this.canvas.getContext( '2d' );

        this.initWaterRectangles();

        this.canvasClickListener = this.renderer.listen( this.canvas, 'click', ( event: MouseEvent ) => {
            event.stopPropagation();
            console.log( 'X: ' + event.offsetX, 'Y: ' + event.offsetY );
            const rect = this.getClickedRectangle( event.offsetX, event.offsetY );
            console.table( rect );
        } );

        this.canvasDragoverListener = this.renderer.listen( this.canvas, 'dragover', ( event: DragEvent ) => {
            event.stopPropagation();
            event.preventDefault();
        } )

        this.canvasDropListener = this.renderer.listen( this.canvas, 'drop', ( event: any ) => {
            event.stopPropagation();
            let droppedShip: any;
            try {
                droppedShip = JSON.parse( event.dataTransfer.getData( 'shipData' ) );
            } catch ( error ) {
                console.error( error );
                droppedShip = null;
            }
            if ( droppedShip !== null ) {
                let x: number = event.offsetX;
                let y: number = event.offsetY;
                let startingRect = this.getClickedRectangle( x, y );
                this.drawShip( droppedShip, startingRect );
            }
        } )

        this.emitQuantities();
    }

    private initWaterRectangles(): void {
        this.waterRectangles.length = 0;
        this.ships.length = 0;

        for ( let i = 0; i < this.RECT_PER_SIDE; i++ ) {
            for ( let j = 0; j < this.RECT_PER_SIDE; j++ ) {
                this.waterRectangles.push( {
                    x: i * ( this.RECT_SIZE ),
                    y: j * ( this.RECT_SIZE ),
                    width: this.RECT_SIZE,
                    height: this.RECT_SIZE,
                    ship: null,
                    hit: false
                } )
            }
        }

        this.drawWaterRectangles();
    }

    public openGames(): void {
        this.readyToChooseGame.emit( { msg: 'showGames' } );
    }

    private initQuantities(): void {
        for ( let i = 0; i < this.shipQuantities.length; i++ ) {
            this.shipQuantities[ i ] = this.shipQuantities.length - i;
        }
    }

    /**
     *
     * Draws the watter rectangles. 
     * @private
     *
     * @memberOf CanvasComponent
     */
    private drawWaterRectangles(): void {
        this.waterRectangles.forEach(( rectangle: any ) => {
            const { x, y, width, height } = rectangle;
            this.context.fillStyle = '#00bfff';
            this.context.fillRect( x, y, width, height );
            this.context.strokeStyle = '#769abe';
            this.context.lineWidth = 1;
            this.context.strokeRect( x, y, width, height );
        } )
    }

    private drawShip( ship: any, startingRect: any ): any {
        const width: number = ship.horizontal ? ship.size * this.RECT_SIZE : this.RECT_SIZE;
        const height: number = ship.horizontal ? this.RECT_SIZE : ship.size * this.RECT_SIZE;
        if ( this.checkIfShipCanBePalced( ship, startingRect ) ) {
            let newShip: any;
            this.context.fillStyle = '#00008b';
            this.context.fillRect( startingRect.rectangleRef.x, startingRect.rectangleRef.y, width, height );
            newShip = {
                size: ship.size,
                damage: 0,
                isSank: false,
                horizontal: ship.horizontal,
                startingRectIndex: startingRect.arrayIndex
            }

            this.ships.push( newShip );
            for ( let i = 0; i < ship.size; i++ ) {
                if ( ship.horizontal ) {
                    this.waterRectangles[ startingRect.arrayIndex + i * this.RECT_PER_SIDE ].ship = newShip;
                } else {
                    this.waterRectangles[ startingRect.arrayIndex + i ].ship = newShip;
                }
            }
            this.shipQuantities[ ship.size - 1 ]--;
            this.emitQuantities();

            console.log( this.ships );
            console.log( this.waterRectangles );
        }
    }

    private checkIfShipCanBePalced( ship: any, startingRect: any ): boolean {
        const width: number = ship.horizontal ? ship.size * this.RECT_SIZE : this.RECT_SIZE;
        const height: number = ship.horizontal ? this.RECT_SIZE : ship.size * this.RECT_SIZE;
        if ( ship.horizontal ) {
            if ( startingRect.rectangleRef.x + width <= this.RECT_SIZE * this.RECT_PER_SIDE ) {
                for ( let i = 1; i >= -1; i-- ) {
                    const minHorizontalArrayIndex: number = startingRect.arrayIndex - this.RECT_PER_SIDE - i;
                    const maxHorizontalArrayIndex: number = startingRect.arrayIndex + ship.size * this.RECT_PER_SIDE - i;
                    for ( let j = minHorizontalArrayIndex; j <= maxHorizontalArrayIndex; j += this.RECT_PER_SIDE ) {
                        if ( j >= 0 && j < 99 && this.waterRectangles[ j ].ship !== null ) {
                            return false;
                        }
                    }
                }
                return true;
            }
        } else {
            if ( startingRect.rectangleRef.y + height <= this.RECT_SIZE * this.RECT_PER_SIDE ) {
                for ( let i = 1; i >= -1; i-- ) {
                    const minVerticalArrayIndex: number = startingRect.arrayIndex - this.RECT_PER_SIDE * i - i;
                    const maxVerticalArrayIndex: number = startingRect.arrayIndex - this.RECT_PER_SIDE * i + ship.size + 1;
                    for ( let j = minVerticalArrayIndex; j <= maxVerticalArrayIndex; j++ ) {
                        if ( j >= 0 && j < 99 && this.waterRectangles[ j ].ship !== null ) {
                            return false;
                        }
                    }
                }



                return true;
            }
        }
    }

    /**
     * 
     * Returns the coordinates, albel and reference to
     * rectangle.
     * @private
     * @param {number} x 
     * @param {number} y 
     * @returns {*} 
     * 
     * @memberOf CanvasComponent
     */
    private getClickedRectangle( x: number, y: number ): any {
        const letter: string = this.X_LABELS[ Math.ceil( x / this.RECT_SIZE ) - 1 ];
        const index: number = ( ( this.RECT_PER_SIDE + 1 ) - Math.ceil( y / this.RECT_SIZE ) );
        const indexX: number = Math.ceil( x / this.RECT_SIZE );
        const indexY: number = Math.ceil( y / this.RECT_SIZE );
        const arrayIndex: number = ( indexX - 1 ) * this.RECT_PER_SIDE + indexY - 1;
        return {
            indexX: indexX,
            indexY: indexY,
            label: letter + index,
            arrayIndex: arrayIndex,
            rectangleRef: this.waterRectangles[ arrayIndex ]
        }
    }

    private emitQuantities(): void {
        this.shipService.shipEmitter.emit( {
            type: 'shipQuantities',
            data: this.shipQuantities
        } )
    }

    public redeploy(): void {
        this.context.clearRect( 0, 0, 400, 400 );
        this.initQuantities();
        this.initWaterRectangles();
        this.emitQuantities();
    }


  
    /**
     * 
     * Clear event listener.
     * 
     * @memberOf CanvasComponent
     */
    public ngOnDestroy(): void {
        this.canvasClickListener();
        this.canvasDragoverListener();
        this.canvasDropListener();
    }
}
