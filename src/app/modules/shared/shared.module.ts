import {
    NgModule
} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import {
   CanvasComponent
} from './index';

@NgModule( {
    imports: [
        BrowserModule      
    ],
    declarations: [
      CanvasComponent
    ],
    exports: [
      CanvasComponent
    ],
    providers: [
      
    ]
})
export class SharedModule { }
