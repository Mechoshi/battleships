import { ShipService } from '../../../shared/services/ship.service';
import {
    Component
} from '@angular/core';

@Component( {
    selector: "mp-select-ship",
    templateUrl: "./select-ship.component.html",
    host: {
        'class': 'select-ship-container marg-left'
    }
} )
export class SelectShipComponent {
    private SHIP_LABELS: string[] = [ 'Frigate', 'Submarine', 'Battle Cruiser', 'Aircraft Carrier' ]
    private shipQuantities: number[];

    constructor( private shipService: ShipService ) { }

    ngOnInit(): void {
        if ( !this.shipQuantities ) {
            this.shipService.shipEmitter.subscribe(( msg: any ) => {
                if ( msg.type === 'shipQuantities' ) {
                    this.shipQuantities = msg.data;
                }
            } )
        }

    }
}