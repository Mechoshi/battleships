
import {
    AfterViewInit,
    Component,
    ElementRef,
    Input,
    OnDestroy,
    OnInit,
    Renderer
} from '@angular/core';

import { ShipService } from '../../../shared/';

@Component( {
    selector: "mp-ship",
    templateUrl: "./ship.component.html",
    host: {
        'class': 'ship-container'
    }
} )
export class ShipComponent implements OnInit, AfterViewInit, OnDestroy {
    private SHIP_LABELS: string[] = [ 'Frigate', 'Submarine', 'Battle Cruiser', 'Aircraft Carrier' ]

    private SIDE_SIZE: number = 40;
    private element: HTMLElement;
    private label: string;
    private dragStartListener: Function;
    private canBeDeployed: boolean = true;
    @Input( 'size' ) private size: number;
    @Input( 'horizontal' ) private horizontal: boolean;

    constructor(
        private elementRef: ElementRef,
        private renderer: Renderer,
        private shipService: ShipService
    ) { }

    public ngOnInit(): void {
        if ( this.horizontal ) {
            this.label = this.SHIP_LABELS[ this.size - 1 ];
        }
    }

    public ngAfterViewInit(): void {
        this.element = this.elementRef.nativeElement;
        if ( this.horizontal ) {
            this.renderer.setElementStyle( this.element, 'width', `${ this.size * this.SIDE_SIZE }px` );
            this.renderer.setElementStyle( this.element, 'height', `${ this.SIDE_SIZE }px` );
            this.renderer.setElementStyle( this.element, 'lineHeight', `${ this.SIDE_SIZE }px` );
        } else {
            this.renderer.setElementStyle( this.element, 'width', `${ this.SIDE_SIZE }px` );
            this.renderer.setElementStyle( this.element, 'height', `${ this.size * this.SIDE_SIZE }px` );
        }

        this.renderer.setElementAttribute( this.element, 'draggable', 'true' );

        this.dragStartListener = this.renderer.listen( this.element, 'dragstart', ( event: DragEvent ) => {
            event.stopPropagation();
            if ( !this.canBeDeployed || event.offsetX > this.SIDE_SIZE || event.offsetY > this.SIDE_SIZE ) {
                event.preventDefault();
            }
            const shipData: Object = {
                size: this.size,
                horizontal: this.horizontal
            }
            event.dataTransfer.setData( 'shipData', JSON.stringify( shipData ) );
        } )

        this.shipService.shipEmitter.subscribe(( msg: any ) => {
            if ( msg.type === 'shipQuantities' ) {
                this.canBeDeployed = msg.data[ this.size - 1 ] !== 0;
                if ( this.canBeDeployed ) {
                    this.renderer.setElementStyle( this.element, 'visibility', 'visible' );
                } else {
                    this.renderer.setElementStyle( this.element, 'visibility', 'hidden' );
                }
            }
        } )


    }

    public ngOnDestroy(): void {
        this.dragStartListener();
    }
}