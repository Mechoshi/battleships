import {
    NgModule
} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import {
    SelectShipComponent,
    ShipComponent
} from './index';

@NgModule( {
    imports: [
        BrowserModule
    ],
    declarations: [
        SelectShipComponent,
        ShipComponent
    ],
    exports: [
        SelectShipComponent,
        ShipComponent
    ],
    providers: [

    ]
} )
export class ShipsModule { }
