import {
    ShipService,
    SocketService
} from './modules/shared';
export const providers: any[] = [
    ShipService,
    SocketService
]