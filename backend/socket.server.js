/**
 * Importing APIs
 */
const express = require( 'express' );
const http = require( 'http' );
const socketIo = require( 'socket.io' );

/**
 * Constants declarations
 */
const app = express();
const server = http.createServer( app );
const PORT = 3000;
const io = socketIo( server );
let sockets = [];
let games = [];
let startedGames = [];
let counter = 0;
/**
 * Handler functions
 */
function onConnect( socket ) {

    console.log( socket.id );
    sockets.push( socket );
    socket.emit( 'getGames', games );
    socket.on( 'fromClient', ( data ) => {
        sockets.forEach( soc => {
            soc.emit( 'message', data );
        } )
        // io.emit('message', data);
    } );
    socket.on( 'createGame', ( data ) => {
        data[ 'player1' ] = data.id;
        games.push( data );
        sockets.forEach( soc => {
            soc.emit( 'newGameAdded', games );
        } )
        // io.emit('message', data);
    } );
    socket.on( 'joinGame', ( data ) => {
        console.log( data );
        games.forEach(( game ) => {
            if ( game.name === data.name ) {
                game[ 'player2' ] = data.id;
            }
        } )

        
        let index;
        for ( let i = 0; i < games.length; i++ ) {
            if ( games[i].name === data.name ) {
                index = i;
            }
        }

        let idx = startedGames.push( games[ index ] ) - 1;
        startedGames[ idx ][ 'idx' ] = idx;
        console.dir(startedGames[idx]);
        console.log( games[ index ].player1 + ' vs. ' + games[ index ].player2 );
        games.splice( index, 1 );

        

        sockets.forEach( soc => {
            if ( soc.id === data.id ) {
                soc.emit( 'startGame', games );
            }

        } )
        // io.emit('message', data);
         sockets.forEach( soc => {
            soc.emit( 'newGameAdded', games );
        } )
    } );
}
function onMessage( data ) {
    console.log( data );
    io.emit( 'message', data );
}

io.on( 'connection', onConnect );

server.listen( PORT );
